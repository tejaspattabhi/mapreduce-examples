import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

public class Q4 {
	public static class Map1 extends Mapper<LongWritable, Text, Text, Text>{
		
		public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
			String[] mydata = value.toString().split("::");
			if (mydata.length > 23) {
				if ("review".compareTo(mydata[22]) == 0) {
					String businessID = mydata[2];
					int stars = Integer.parseInt(mydata[20]);
					Text bID = new Text (businessID);
					Text count = new Text ("A" + stars);
					context.write(bID, count);
				}
			}
		}
	}


	public static class Map2 extends Mapper<LongWritable, Text, Text, Text>{

		public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
			String[] mydata = value.toString().split("::");
			if (mydata.length > 23) {
				String businessID = mydata[2];
				Text bID = new Text (businessID);
				Text thisData = new Text ("B|" + mydata[3] + "\t" + mydata[10]);
				context.write(bID, thisData);
			}
		}
	}

	public static class Reduce extends Reducer<Text, Text, Text, Text> {
		class CompositeWritable {
		    double avg = 0.0;
		    String addrCat = "";
		    
			public double getAvg() {
				return avg;
			}

			public void setAvg(double avg) {
				this.avg = avg;
			}

			public String getAddrCat() {
				return addrCat;
			}

			public void setAddrCat(String addrCat) {
				this.addrCat = addrCat;
			}

			public CompositeWritable() {}

		    public CompositeWritable(double avg, String addrCat) {
		        this.avg = avg;
		        this.addrCat = addrCat;
		    }

		    public String toString() {
		        return (this.avg + "\t" + this.addrCat);
		    }
		}
		
		private java.util.Map<String, CompositeWritable> top10 = new HashMap<String, CompositeWritable>(10);
		
		public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
			int sum = 0; 
			int n = 0;
			double avg = 0.0;
			String addrCat = "";
			boolean bDataPresent = false;
			
			for (Text val : values) {
				if (val.toString().charAt(0) == 'A') {
					sum += Integer.parseInt(val.toString().substring(1));
					n++;
				}
				else if (val.toString().charAt(0) == 'B') {
					String b_val = val.toString().substring(2);
					bDataPresent = true;
					addrCat = b_val;
				}
				else {
					// Do Nothing
				}
			}
			
			if (bDataPresent) {
				avg = (double)Math.round(((sum * 1.0) / n) * 100) / 100;
				CompositeWritable c = new CompositeWritable(avg, addrCat);
				if (this.top10.size() < 10) {
					top10.put(key.toString(), c);
		        } 
				else {
		            for (Entry<String, CompositeWritable> e : this.top10.entrySet()) {
		                if (avg > e.getValue().getAvg()) {
		                    this.top10.remove(e.getKey());
		                    this.top10.put(key.toString(), c);
		                    break;
		                }
		            }
		        }
			}
		}
		
		@Override
		protected void cleanup(Context context) throws IOException, InterruptedException {
			for (Entry<String, CompositeWritable> e : this.top10.entrySet()) {
                context.write(new Text(e.getKey()), new Text (e.getValue().toString()));
            }
	    }
	}
	
	// Driver program
	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
		// get all args
		if (otherArgs.length != 3) {
			System.err.println("Usage: Q4 <Input File 1: review.csv> <Input File 2: business.csv> <Output File Directory>");
			System.exit(2);
		}

		@SuppressWarnings("deprecation")
		Job job = new Job(conf, "Q4"); 
		job.setJarByClass(Q4.class); 
		MultipleInputs.addInputPath(job, new Path(otherArgs[0]), TextInputFormat.class, Map1.class);
		MultipleInputs.addInputPath(job, new Path(otherArgs[1]), TextInputFormat.class, Map2.class);
		job.setReducerClass(Reduce.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);
		FileOutputFormat.setOutputPath(job, new Path(otherArgs[2]));

		//Wait till job completion
		job.waitForCompletion(true);
	}
}
