import java.io.IOException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

public class Q2 {

	public static class Map	extends Mapper<LongWritable, Text, Text, IntWritable>{

		private final static IntWritable one = new IntWritable(1);
		public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
			String[] mydata = value.toString().split("::");
			if (mydata.length > 23 && "business".compareTo(mydata[22])== 0 && mydata[3].contains("Palo")== true) {
				String businessID = mydata[2];
				Text t = new Text (businessID);
				context.write(t, one);
			}
		}
	}

	public static class Reduce extends Reducer<Text,IntWritable,Text,NullWritable> {
		private NullWritable result = NullWritable.get();

		public void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
			context.write(key, result); 
		}
	}
	
	// Driver program
	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
		// get all args
		if (otherArgs.length != 2) {
			System.err.println("Usage: Q2 <Input File: business.csv> <Output File Directory>");
			System.exit(2);
		}

		// create a job with name "Q2" 
		@SuppressWarnings("deprecation")
		Job job = new Job(conf, "Q2"); 
		job.setJarByClass(Q2.class); 
		job.setMapperClass(Map.class);
		job.setReducerClass(Reduce.class);

		// set output key type 
		job.setOutputKeyClass(Text.class);
		// set output value type 
		job.setOutputValueClass(IntWritable.class);
		//set the HDFS path of the input data
		FileInputFormat.addInputPath(job, new Path(otherArgs[0]));
		// set the HDFS path for the output
		FileOutputFormat.setOutputPath(job, new Path(otherArgs[1]));

		//Wait till job completion
		System.exit(job.waitForCompletion(true) ? 0 : 1);
	}
}
