import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

public class Q3 {
	public static class Map	extends Mapper<LongWritable, Text, Text, IntWritable>{

		public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
			String[] mydata = value.toString().split("::");
			if (mydata.length > 23) {
				if ("review".compareTo(mydata[22]) == 0) {
					String businessID = mydata[2];
					int stars = Integer.parseInt(mydata[20]);
					Text t = new Text (businessID);
					IntWritable count = new IntWritable (stars);
					context.write(t, count);
				}
			}
		}
	}

	public static class Reduce extends Reducer<Text, IntWritable, Text, NullWritable> {
		private java.util.Map<String, Double> top10 = new HashMap<String, Double>(10);
		
		public void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
			int sum = 0; 
			int n = 0;
			for (IntWritable val : values) {
				sum += val.get();
				n++;
			}
			
			double avg = (double)Math.round(((sum * 1.0) / n) * 100) / 100;
			
			if (this.top10.size() < 10) {
				top10.put(key.toString(), avg);
	        } 
			else {
	            for (Entry<String, Double> e : this.top10.entrySet()) {
	                if (avg > e.getValue()) {
	                    this.top10.remove(e.getKey());
	                    this.top10.put(key.toString(), avg);
	                    break;
	                }
	            }
	        }
		}
		
		@Override
		protected void cleanup(Context context) throws IOException, InterruptedException {
			for (Entry<String, Double> e : this.top10.entrySet()) {
                context.write(new Text(e.getKey()), NullWritable.get());
            }
	    }
	}
	
	// Driver program
	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
		// get all args
		if (otherArgs.length != 2) {
			System.err.println("Usage: Q3 <Input File: review.csv> <Output File Directory>");
			System.exit(2);
		}

		// create a job with name "Q2" 
		@SuppressWarnings("deprecation")
		Job job = new Job(conf, "Q3"); 
		job.setJarByClass(Q3.class); 
		job.setMapperClass(Map.class);
		job.setReducerClass(Reduce.class);

		job.setOutputKeyClass(Text.class);
		// set output value type 
		job.setOutputValueClass(IntWritable.class);
		//set the HDFS path of the input data
		FileInputFormat.addInputPath(job, new Path(otherArgs[0]));
		// set the HDFS path for the output
		FileOutputFormat.setOutputPath(job, new Path(otherArgs[1]));

		//Wait till job completion
		job.waitForCompletion(true);
	}
}
