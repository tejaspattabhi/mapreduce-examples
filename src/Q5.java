import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

public class Q5 {
	public static class Map extends Mapper<LongWritable, Text, Text, Text>{
		ArrayList<String> sBID = new ArrayList<String>();
		
		public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
			String[] mydata = value.toString().split("::");
			if (mydata.length > 23) {
				if (sBID.contains(mydata[2])) {
					context.write(new Text (mydata[8]), new Text (mydata[1]));
				}
			}
		}
		
		@Override
		protected void setup(Context context) throws IOException, InterruptedException {
			super.setup(context);
			
			// driver code
			@SuppressWarnings("deprecation")
			Path[] localPaths = context.getLocalCacheFiles();
			for (Path myfile : localPaths) {
				String line = "";
				String fileName = myfile.getName();
				File file = new File(fileName + "");
				FileReader fr = new FileReader(file);
				BufferedReader br = new BufferedReader(fr);
				while ((line = br.readLine()) != null) {
					String[] mydata = line.split("::");
					if (mydata.length > 23) {
						if (mydata[12].contains("Stanford")) {
							sBID.add(mydata[2]);
						}
					}
				}
				br.close();
			}
}
	}
	
	// Driver program
	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
		// get all args
		if (otherArgs.length != 3) {
			System.err.println("Usage: Q5 <Input File 1: review.csv> <Input File 2: business.csv> <Output File Directory>");
			System.exit(2);
		}
		
		@SuppressWarnings("deprecation")
		Job job = new Job(conf, "Q5"); 
		job.setJarByClass(Q5.class); 
		job.setNumReduceTasks(0);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);
		
		MultipleInputs.addInputPath(job, new Path(otherArgs[0]), TextInputFormat.class, Map.class);
		final String node = "hdfs://sandbox.hortonworks.com:8020";
		job.addCacheFile(new URI (node + "/user/hue/" + otherArgs[1]));
		FileOutputFormat.setOutputPath(job, new Path(otherArgs[2]));
		
		job.waitForCompletion(true);
	}
}
