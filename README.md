Big Data Management and Analytics
==
Project Coded in Java
--

#### Please follow the steps for execution:

**Step 1:** Export the project to **Q.jar**. Push the JAR file to a Hortonworks VM
		
**Step 2:** Add the input files to HDFS using the following commands

		> hdfs dfs -put data.csv # Old Data Set - Used only for Q1
		> hdfs dfs -put business.csv
		> hdfs dfs -put review.csv
		> hdfs dfs -put user.csv
		
**Step 3:** For Each Question, the Jar Name is Q[Question Number].jar and class is Q[Question Number]. 
		Use the following to execute each JAR

		> hadoop jar Q.jar Q1 data.csv Q1
		> hadoop jar Q.jar Q2 business.csv Q2
		> hadoop jar Q.jar Q3 review.csv Q3
		> hadoop jar Q.jar Q4 review.csv business.csv Q4
		> hadoop jar Q.jar Q5 review.csv business.csv Q5

**Step 4:** To View Output, please use the following commands:

		> hdfs dfs -cat Q1/*
		> hdfs dfs -cat Q2/*
		> hdfs dfs -cat Q3/*
		> hdfs dfs -cat Q4/*
		> hdfs dfs -cat Q5/*

***Note:*** The Project Needs External JARs supporting Hadoop and MapReduce. 

*Please contact me on any kind of concerns/questions*

**By,** 
### Tejas Tovinkere Pattabhi 
**Email: tejas.pattabhi@gmail.com**

**Net ID: txp130630**